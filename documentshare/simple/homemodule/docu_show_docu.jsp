<%@page import="net.simpleframework.content.ContentUtils"%>
<%@page import="net.simpleframework.content.news.NewsUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.core.ado.IDataObjectQuery"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%>
<%@page import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.simpleframework.web.WebUtils"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="net.simpleframework.util.DateUtils"%>

<%
	final String _docu_type = WebUtils.toLocaleString(request.getParameter("_docu_type"));
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IDataObjectQuery<?> qs = DocuUtils.queryDocu(requestResponse, null, "docu", _docu_type);
	final int rows = ConvertUtils.toInt(request.getParameter("rows"), 6);
	if (qs == null) {
		return;
	}
%>
<div class="hot-img">
	<%
		DocuBean docuBean;
		while ((docuBean = (DocuBean) qs.next()) != null) {
			if (qs.position() >= rows) {
				break;
			}
	%>
	<dl class="mt5">
		<dt>
			<a
				href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
				target="_blank"><img
					src="<%=DocuUtils.getPageImgSrc(requestResponse, docuBean)%>"
					title="<%=docuBean.getTitle()%>" alt="<%=docuBean.getTitle()%>">
			</a>
			<span class="icon1" title="<%=docuBean.getExtension()%>"
				style="background-image: url('<%=DocuUtils.getFileImage(requestResponse, docuBean)%>');"></span>
		</dt>
		<dd>
			<a
				href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
				target="_blank"><%=ItSiteUtil.getShortString(docuBean.getTitle(), 20, true)%></a>
		</dd>
		<%
			if ("new".equals(_docu_type)) {
		%>
		<dd class="f-grey">
			发布:<%=DateUtils.getRelativeDate(docuBean.getCreateDate())%>
		</dd>
		<%
			}
		%>
		<dd class="f-grey">
			用户:<%=ContentUtils.getAccountAware().wrapAccountHref(requestResponse, docuBean.getUserId(), docuBean.getUserText())%>
		</dd>
	</dl>
	<%
		}
	%>
</div>
<style>
.hot-img dl {
	float: left;
	margin-right: 15px;
}
</style>