<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.documentshare.common.CommonUtils"%>
<%@page import="net.documentshare.common.ECommonType"%>

<div id="commonForm">
	<table>
		<tr>
			<td colspan="2">
				<input type="button" value="保存" onclick="$Actions['notifyAct']();">
				<input type="button" value="预览"
					onclick="$Actions['notifyShowAct']();">
			</td>
		</tr>
		<tr>
			<td align="right" width="60%">
				<div class="clear_float" style="padding-bottom: 3px;">
					<div style="float: left" id="notifyId_info" class="important-tip">
						#(CKEditor.0)
					</div>
				</div>
				<div>
					<textarea id="notifyId" name="notifyId" style="display: none;"></textarea>
				</div>
			</td>
			<td align="right" width="40%" valign="top">
				<fieldset>
					<legend>
						预览效果
					</legend>
					<div id="notifyHtmlId" align="left"><%=CommonUtils.getContent(ECommonType.notify)%></div>
				</fieldset>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
$ready(function() {
	window.onbeforeunload = function() {
		return "#(CKEditor.1)";
	};
});
</script>
