<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<%@ page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.question.remark.QuestionRemarkUtils"%>

<%
	final ComponentParameter nComponentParameter = new ComponentParameter(request, response, null);
	long count = ConvertUtils.toLong(request.getAttribute("count"), 0);
	if (count > 0) {
%>
<div class="list">
	<div class="title"><%="答案 (" + count + "个答案)"%></div>
	<%
		}
		out.write(QuestionRemarkUtils.itemsHtml(nComponentParameter));
	%>
</div>
<script type="text/javascript">
function __remark_window(o, params) {
	$Actions["ajaxRemarkEditPage"].selector = "#remark_tb_";
	$Actions["remarkEditWindow"](params);
}
</script>