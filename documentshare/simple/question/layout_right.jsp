<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@page import="net.simpleframework.util.ConvertUtils"%>
<%@page import="net.simpleframework.organization.account.IAccount"%>
<%@page import="net.simpleframework.organization.OrgUtils"%>
<%@page import="net.simpleframework.content.ContentUtils"%>
<%@page import="net.documentshare.mytag.ETagType"%><%@page
	import="net.documentshare.question.QuestionUtils"%><%@page
	import="net.documentshare.mytag.MyTagUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.content.IContentPagerHandle"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String tags_params = IContentPagerHandle._VTYPE + "=" + ETagType.question.ordinal();
	final String tags_layout = MyTagUtils.deploy + "/tags_layout.jsp?" + tags_params + "&t="
			+ StringUtils.text(requestResponse.getRequestParameter("t"), "all") + "&od="
			+ StringUtils.text(requestResponse.getRequestParameter("od"), "new");
%>
<div class="block_layout2">
	<div class="t f4">
		热门标签
	</div>
	<div class="wrap_text"><jsp:include page="<%=tags_layout%>"
			flush="true">
			<jsp:param value="50" name="rows" />
		</jsp:include></div>
</div>
<div class="block_layout1">
	<div class="t f4">
		活跃用户
	</div>
	<div class="wrap_text">
		<jsp:include page="activity_user.jsp" flush="true">
			<jsp:param value="false" name="showMore" />
			<jsp:param value="dot1" name="dot" />
			<jsp:param value="40" name="rows" />
		</jsp:include>
	</div>
</div>