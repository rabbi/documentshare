<%@page import="net.simpleframework.content.news.NewsUtils"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.core.ado.IDataObjectQuery"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%><%@page
	import="net.documentshare.question.QuestionBean"%><%@page import="net.documentshare.question.QuestionUtils"%><%@page import="net.documentshare.utils.ItSiteUtil"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IDataObjectQuery<?> qs = ContentLayoutUtils.getQueryByRequest(requestResponse);
	final String questionId = request.getParameter("questionId");
	if (qs == null) {
		return;
	}
%>
<div class="list_layout">
	<%
		QuestionBean questionBean;
		while ((questionBean = (QuestionBean) qs.next()) != null) {
			if (questionBean.getId().equals2(questionId)) {
				continue;
			}
	%>
	<div class="rrow"
		style="background: url('<%=ContentLayoutUtils.dotImagePath(requestResponse, NewsUtils.deployPath)%>') 5px 5px no-repeat;">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td><%=QuestionUtils.wrapOpenLink(requestResponse, questionBean)%></td>
				<td align="right"><%=ItSiteUtil.layoutRemarkAndView(questionBean, false)%>
				</td>
			</tr>
		</table>
		<span class="nnum"> <%=ItSiteUtil.layoutTime(requestResponse, questionBean, "yyyy-MM-dd", true)%>
		</span>
	</div>
	<%
		}
	%>
</div>