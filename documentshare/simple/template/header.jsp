<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="java.util.Map"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.web.WebUtils"%><%@page
	import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.ESearchFunction"%>

<style>
.radio {
	vertical-align: middle;
	margin: -2px 0px 3px 0px;
}

.label {
	padding-right: 5px;
}
</style>
<div class="logobar clear_float">
	<div style="float: right;" id="t_header_tb">
		<table>
			<tr>
				<td><jsp:include page="header_tb.jsp" flush="true"></jsp:include>
				</td>
			</tr>
			<tr>
				<td style="padding-top: 5px;">
					<input type="text" name="_search_topic" id="sinput"
						style="color: #666;"
						onclick="if(this.value=='搜索...')this.value='';"
						value="<%=WebUtils.toLocaleString(StringUtils.text(request.getParameter("_search_topic"), "搜索..."))%>">
					<input value="搜索" type="button" class="sbtn" id="searchBtnId"
						style="background-color: whiteSmoke; line-height: 28px; height: 30px; -webkit-border-radius: 0px; -moz-border-radius: 0px; border-radius: 0px;"
						onclick="doSearch();">
					<div align="left">
						<%
							final String s = StringUtils.text(request.getParameter("_search_function"), "docu");
							for (final ESearchFunction d : ESearchFunction.values()) {
						%>
						<input type="radio"
							<%=d.name().equals(s) ? "checked=\"checked\"" : ""%>
							value="<%=d.name()%>" id="<%=d.name()%>" name="_search_function"
							class="radio">
						<label class="label" for="<%=d.name()%>"><%=d.toString()%></label>
						<%
							}
						%>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<a class="logo" href="/"></a>
</div>
<div class="menubar clear_float">
	<div style="float: left; padding-left: 20px;" id="_simple_menu"></div>
</div>
<jsp:include page="head_script.jsp"></jsp:include>
<script type="text/javascript">
function doSearch() {
	$Actions.loc('/simple/template/search.jsp?' + $$Form('.logobar'));
}
$Comp.addReturnEvent($('sinput'), function(ev) {
	var v = $F('sinput');
	if (v == "") {
		return;
	}
	doSearch();
});
</script>