<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.docu.DocuUtils"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String id = StringUtils.text(request.getParameter("id"), "docu");
%>
<div class="simple_toolbar1">
	<%
		if (ItSiteUtil.isManage(requestResponse, DocuUtils.applicationModule)) {
	%>
	<input type="button" value="复原目录"
		onclick="$IT.A('revertCatalogAct','id=<%=id%>');">
	<%
		}
	%>
</div>
<div class="simple_toolbar1" id="catalogId">
</div>
<script type="text/javascript">
$ready(function() {
	if ('docu' == '<%=id%>') {
		$IT.A('docuCatalogAct');
	} else if ('code' == '<%=id%>') {
		$IT.A('docuCodeCatalogAct');
	}
});
</script>