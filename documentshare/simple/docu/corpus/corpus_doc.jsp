<%@page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.web.page.component.ui.pager.PagerUtils"%>
<%@ page import="java.util.List"%><%@page
	import="net.simpleframework.util.HTMLBuilder"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.util.DateUtils"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	String setFront = request.getParameter("setFront");
	int height = 210;
	if (StringUtils.hasText(setFront)) {
		height = 235;
	}
%>
<div class="hot-img" style="height: <%=height%>px;">
	<%
		final List<?> data = PagerUtils.getPagerList(request);
		if (data == null && data.size() == 0) {
			return;
		}
		for (Object o : data) {
			final DocuBean docuBean = (DocuBean) o;
	%>
	<dl class="mt5">
		<dt>
			<a
				href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
				target="_blank"><img
					src="<%=DocuUtils.getPageImgSrc(requestResponse, docuBean)%>"
					title="<%=docuBean.getTitle()%>" alt="<%=docuBean.getTitle()%>">
			</a>
			<span class="icon" title="<%=docuBean.getExtension()%>"
				style="background-image: url('<%=DocuUtils.getFileImage(requestResponse, docuBean)%>');"></span>
		</dt>
		<dd>
			<a
				href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
				target="_blank"><%=ItSiteUtil.getShortString(docuBean.getTitle(), 6, true)%></a>
		</dd>
		<dd class="f-grey">
			阅读:<%=docuBean.getViews()%>
		</dd>
		<dd class="f-grey">
			发布:<%=DateUtils.getRelativeDate(docuBean.getCreateDate())%>
		</dd>
		<dd class="f-grey">
			用户:<%=ContentUtils.getAccountAware().wrapAccountHref(requestResponse, docuBean.getUserId(), docuBean.getUserText())%>
		</dd>
		<dd class="f-grey">
			<input type="radio" name="frontCoverId" 
				value="<%=docuBean.getId().getValue()%>">
		</dd>
	</dl>
	<%
		}
	%>
</div>
