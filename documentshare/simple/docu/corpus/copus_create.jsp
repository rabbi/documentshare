<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
#__np__newsAddForm #corpus_title,#__np__newsAddForm #description,#td_docu_catalog .text
	{
	border-width: 0;
	width: 99%;
	background-image: none;
}
</style>
<div id="addCorpusForm">
	<input type="hidden" name="corpos_Id" id="corpos_Id">
	<div id="__np__newsAddForm">
		<table cellspacing="0" class="tbl tbl_first">
			<tr>
				<td class="lbl">
					文辑标题:
				</td>
				<td>
					<input type="text" id="corpus_title" name="corpus_title" />
				</td>
			</tr>
		</table>
		<table cellspacing="0" class="tbl">
			<tr>
				<td class="lbl">
					文辑简介:
				</td>
				<td>
					<textarea rows="10" cols="" name="description" id='description'></textarea>
				</td>
			</tr>
		</table>
		<table class="tbl" cellspacing="0">
			<tr>
				<td class="lbl">
					分类：
				</td>
				<td>
					<div id="td_docu_catalog" style="width: 99%"></div>
					<input type="hidden" name="docu_catalog" id="docu_catalog">
				</td>
			</tr>
		</table>
		<table class="tbl" cellspacing="0">
			<tr>
				<td align="right" style="padding: 2px;">
					<input type="button" class="button2" value="保存" id="add_corpus"
						onclick="$Actions['docu_corpusSaveAct']()" />
				</td>
			</tr>
		</table>
	</div>
</div>
<script type="text/javascript">
	$ready(function() {
		addTextButton("docu_catalog_text", "docuCatalogAct", "td_docu_catalog",
				false);
	});
</script>