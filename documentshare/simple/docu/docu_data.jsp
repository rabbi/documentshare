<%@page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.web.page.component.ui.pager.PagerUtils"%>
<%@ page import="java.util.List"%><%@page
	import="net.simpleframework.util.HTMLBuilder"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.util.DateUtils"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="net.simpleframework.util.ConvertUtils"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final int s = ConvertUtils.toInt(requestResponse.getRequestParameter("tt"), 1);
	if (s == 0 || s == 1) {
%>
<div class="hot-img">
	<%
		final List<?> data = PagerUtils.getPagerList(request);
			if (data == null && data.size() == 0) {
				return;
			}
			for (Object o : data) {
				final DocuBean docuBean = (DocuBean) o;
	%>
	<dl class="mt5">
		<dt>
			<a
				href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
				target="_blank"><img
					src="<%=DocuUtils.getPageImgSrc(requestResponse, docuBean)%>"
					title="<%=docuBean.getTitle()%>" alt="<%=docuBean.getTitle()%>">
			</a>
			<span class="icon1" title="<%=docuBean.getExtension()%>"
				style="background-image: url('<%=DocuUtils.getFileImage(requestResponse, docuBean)%>');"></span>
		</dt>
		<dd>
				<a
				href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>" title="<%=docuBean.getTitle()%>"
				target="_blank"><%=ItSiteUtil.getShortString(docuBean.getTitle(), 20, true)%></a>
		</dd>
		<dd class="f-grey">
			阅读:<%=docuBean.getViews()%>
		</dd>
		<dd class="f-grey">
			发布:<%=DateUtils.getRelativeDate(docuBean.getCreateDate())%>
		</dd>
		<dd class="f-grey">
			用户:<%=ContentUtils.getAccountAware().wrapAccountHref(requestResponse, docuBean.getUserId(), docuBean.getUserText())%>
		</dd>
	</dl>
	<%
		}
	%>
</div>
<%
	} else {
%>
<div class="list_layout" style="float: left;display: block;width: 100%;">
	<%
		String show = request.getParameter("show");
		final List<?> data = PagerUtils.getPagerList(request);
			if (data == null && data.size() == 0) {
				return;
			}
			for (Object o : data) {
				final DocuBean docuBean = (DocuBean) o;
	%>
	<div class="rrow" style="padding-left: 0px;float: left;width: 48%;" >
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20">
					<img alt="<%=docuBean.getExtension()%>"
						src="<%=DocuUtils.getFileImage(requestResponse, docuBean)%>">
				</td>

				<td>
					<a
						href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
						target="_blank"><%=ItSiteUtil.getShortString(docuBean.getTitle(),25,true)%></a>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
	<%if(StringUtils.hasText(show)){ %>
	<script type="text/javascript">
		$IT.hidden('#<%=show%> .rrow', '<%=show%>_');
	</script>
	<%} %>
</div>

<%
	}
%>
<script type="text/javascript">
$ready(function() {
	if ($('special_docu'))
		$('special_docu').innerHTML = '<%=EDocuFunction.whichOrdinal(s-1)%>';
});
</script>