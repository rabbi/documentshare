package net.simpleframework.content.news;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.applets.attention.AttentionUtils;
import net.simpleframework.applets.tag.ITagApplicationModule;
import net.simpleframework.applets.tag.TagBean;
import net.simpleframework.applets.tag.TagUtils;
import net.simpleframework.content.ContentUtils;
import net.simpleframework.content.EContentStatus;
import net.simpleframework.content.EContentType;
import net.simpleframework.content.component.newspager.DefaultNewsPagerHandle;
import net.simpleframework.content.component.newspager.NewsBean;
import net.simpleframework.content.component.newspager.NewsCatalog;
import net.simpleframework.content.component.newspager.NewsPagerUtils;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.core.bean.IDataObjectBean;
import net.simpleframework.core.bean.IIdBeanAware;
import net.simpleframework.my.space.MySpaceUtils;
import net.simpleframework.organization.IJob;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.organization.account.AccountContext;
import net.simpleframework.organization.account.AccountSession;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.DateUtils;
import net.simpleframework.util.HTMLBuilder;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.EFunctionModule;
import net.simpleframework.web.IWebApplicationModule;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.PageUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;
import net.simpleframework.web.page.component.ui.pager.TablePagerColumn;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class NewsPagerHandle extends DefaultNewsPagerHandle {

	@Override
	public Object getBeanProperty(final ComponentParameter compParameter, final String beanProperty) {
		if ("showCheckbox".equals(beanProperty)) {
			return IWebApplicationModule.Utils.isManager(compParameter, getApplicationModule());
		} else {
			if ("jobView".equals(beanProperty)) {
				return IJob.sj_anonymous;
			} else if (beanProperty != null && beanProperty.startsWith("job")) {
				return IJob.sj_account_normal;
			}
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public Class<? extends IIdBeanAware> getEntityBeanClass() {
		return News.class;
	}

	@Override
	public String getViewUrl(final ComponentParameter compParameter, final NewsBean news) {
		return getApplicationModule().getViewUrl(compParameter, news);
	}

	@Override
	public <T extends IDataObjectBean> void doBeforeAdd(final ComponentParameter compParameter,
			final ITableEntityManager temgr, final T t, final Map<String, Object> data,
			final Class<T> beanClazz) {
		super.doBeforeAdd(compParameter, temgr, t, data, beanClazz);
		final News news = (News) t;
		if (IWebApplicationModule.Utils.isManager(compParameter, getApplicationModule())) {
			news.setStatus(EContentStatus.publish);
		}
	}

	@Override
	public <T extends IDataObjectBean> void doEditCallback(final ComponentParameter compParameter,
			final ITableEntityManager temgr, final T t, final Map<String, Object> data,
			final Class<T> beanClazz) {
		super.doEditCallback(compParameter, temgr, t, data, beanClazz);

		if (data.get("status") == EContentStatus.publish) {
			final News news = (News) t;
			AccountContext.update(OrgUtils.am().queryForObjectById(news.getUserId()),
					"news_statusPublish", news.getId());
		}
	}

	@Override
	public <T extends IDataObjectBean> void doAddCallback(final ComponentParameter compParameter,
			final ITableEntityManager temgr, final T t, final Map<String, Object> data,
			final Class<T> beanClazz) {
		super.doAddCallback(compParameter, temgr, t, data, beanClazz);
		final News news = (News) t;

		final IAccount account = OrgUtils.am().queryForObjectById(news.getUserId());
		if (news.getStatus() == EContentStatus.publish) {
			AccountContext.update(account, "news_statusPublish", news.getId());
		}
		MySpaceUtils.addSapceLog(compParameter, null, EFunctionModule.news, news.getId());
	}

	@Override
	public String getNavigateHTML(final ComponentParameter compParameter) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<a href=\"").append(getApplicationModule().getApplicationUrl(compParameter))
				.append("\">");
		sb.append(getApplicationModule().getApplicationText()).append("</a>");
		final NewsBean news = getEntityBeanByRequest(compParameter);
		if (news != null) {
			final NewsCatalog catalog = getCatalogById(compParameter, news.getCatalogId());
			if (catalog != null) {
				sb.append(HTMLBuilder.NAV).append("<a href=\"")
						.append(getApplicationModule().getCatalogUrl(compParameter, catalog))
						.append("\">").append(catalog.getText()).append("</a>");
			}
			sb.append(HTMLBuilder.NAV).append(LocaleI18n.getMessage("DefaultNewsPagerHandle.0"));
		}
		final String time = compParameter.getRequestParameter("time");
		if (StringUtils.hasText(time)) {
			sb.append(HTMLBuilder.NAV);
			if ("day".equals(time)) {
				sb.append(LocaleI18n.getMessage("ContentUtils.time.1"));
			} else if ("day2".equals(time)) {
				sb.append(LocaleI18n.getMessage("ContentUtils.time.2"));
			} else if ("week".equals(time)) {
				sb.append(LocaleI18n.getMessage("ContentUtils.time.3"));
			} else if ("month".equals(time)) {
				sb.append(LocaleI18n.getMessage("ContentUtils.time.4"));
			} else if ("month3".equals(time)) {
				sb.append(LocaleI18n.getMessage("ContentUtils.time.5"));
			}
		}
		final String status = compParameter.getRequestParameter("status");
		if (StringUtils.hasText(status)) {
			sb.append(HTMLBuilder.NAV);
			if (status.equals(EContentStatus.edit.name())) {
				sb.append(EContentStatus.edit);
			} else if (status.equals(EContentStatus.audit.name())) {
				sb.append(EContentStatus.audit);
			} else if (status.equals(EContentStatus.publish.name())) {
				sb.append(EContentStatus.publish);
			} else if (status.equals(EContentStatus.lock.name())) {
				sb.append(EContentStatus.lock);
			} else if (status.equals(EContentStatus.delete.name())) {
				sb.append(EContentStatus.delete);
			}
		}

		final int catalog = ConvertUtils.toInt(getCatalogId(compParameter), -1);
		final List<NewsCatalog> catalogs;
		if (catalog > -1 && (catalogs = listNewsCatalog(compParameter)) != null) {
			if (catalog < catalogs.size()) {
				sb.append(HTMLBuilder.NAV);
				sb.append(catalogs.get(catalog).getText());
			}
		}

		final TagBean tagBean = TagUtils.getTagBeanById(compParameter
				.getRequestParameter(ITagApplicationModule._TAG_ID));
		if (tagBean != null) {
			sb.append(HTMLBuilder.NAV);
			sb.append(tagBean.getTagText());
			TagUtils.updateViews(compParameter, tagBean);
		}
		wrapNavImage(compParameter, sb);
		return sb.toString();
	}

	@Override
	public Map<String, Object> getFormParameters(final ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "time");
		putParameter(compParameter, parameters, "c");
		putParameter(compParameter, parameters, "status");
		putParameter(compParameter, parameters, "t");
		putParameter(compParameter, parameters, ITagApplicationModule._TAG_ID);
		return parameters;
	}

	@Override
	public INewsApplicationModule getApplicationModule() {
		return NewsUtils.applicationModule;
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final String c = PageUtils.toLocaleString(compParameter.getRequestParameter("c"));
		if (StringUtils.hasText(c)) {
			return createLuceneManager(compParameter).getLuceneQuery(c);
		}
		final ITableEntityManager news_mgr = getTableEntityManager(compParameter);
		final StringBuilder sql = new StringBuilder();
		final ArrayList<Object> al = new ArrayList<Object>();
		final String tag = compParameter.getRequestParameter(ITagApplicationModule._TAG_ID);
		if (StringUtils.hasText(tag)) {
			appendTagsSQL(compParameter, sql, al, tag);
			sql.append(" where ");
			initStatusSQL(compParameter, sql, al, "a");
			sql.append(" order by a.ttop desc, a.createdate desc");
			return news_mgr.query(new SQLValue(sql.toString(), al.toArray()), getEntityBeanClass());
		}

		final String t = compParameter.getRequestParameter("t");
		if ("attention".equals(t)) {
			return news_mgr.query(
					AttentionUtils.attentionSQLValue(compParameter, getFunctionModule()),
					getEntityBeanClass());
		}

		initStatusSQL(compParameter, sql, al, null);
		final String time = compParameter.getRequestParameter("time");
		if (StringUtils.hasText(time)) {
			final Calendar cal = DateUtils.getTimeCalendar(time);
			sql.append(" and createdate>?");
			al.add(cal.getTime());
		}
		final int catalog = ConvertUtils.toInt(getCatalogId(compParameter), -1);
		final List<NewsCatalog> catalogs;
		if (catalog > -1 && (catalogs = listNewsCatalog(compParameter)) != null) {
			if (catalog < catalogs.size()) {
				sql.append(" and catalogid=?");
				al.add(catalogs.get(catalog).getId());
			}
		}
		if ("recommended".equals(t)) {
			sql.append(" and ttype=?");
			al.add(EContentType.recommended);
		}
		sql.append(" order by ttop desc, createdate desc");
		return news_mgr
				.query(new ExpressionValue(sql.toString(), al.toArray()), getEntityBeanClass());
	}

	private void initStatusSQL(final ComponentParameter compParameter, final StringBuilder sql,
			final ArrayList<Object> al, final String tbl) {
		final IAccount account = AccountSession.getLogin(compParameter.getSession());
		if (account == null) {
			if (tbl != null) {
				sql.append(tbl).append(".");
			}
			sql.append("status=?");
			al.add(EContentStatus.publish);
		} else {
			if (IWebApplicationModule.Utils.isManager(compParameter, getApplicationModule())) {
				sql.append("1=1");
				final String status = compParameter.getRequestParameter("status");
				if (StringUtils.hasText(status)) {
					sql.append(" and ");
					if (tbl != null) {
						sql.append(tbl).append(".");
					}
					sql.append("status=?");
					al.add(EContentStatus.valueOf(status));
				}
			} else {
				al.add(account.getId());
				final String status = compParameter.getRequestParameter("status");
				sql.append("(");
				if (tbl != null) {
					sql.append(tbl).append(".");
				}
				if (StringUtils.hasText(status)) {
					sql.append("userid=? and ");
					al.add(EContentStatus.valueOf(status));
				} else {
					sql.append("userid=? or ");
					al.add(EContentStatus.publish);
				}
				if (tbl != null) {
					sql.append(tbl).append(".");
				}
				sql.append("status=?)");
			}
		}
	}

	@Override
	public AbstractTablePagerData createTablePagerData(final ComponentParameter compParameter) {
		return new AppNewsTablePagerData(compParameter);
	}

	class AppNewsTablePagerData extends NewsTablePagerData {
		public AppNewsTablePagerData(final ComponentParameter compParameter) {
			super(compParameter);
		}

		@Override
		public Map<String, TablePagerColumn> getTablePagerColumns() {
			final Map<String, TablePagerColumn> columns = cloneTablePagerColumns();
			if (!IWebApplicationModule.Utils.isManager(compParameter, getApplicationModule())) {
				columns.remove("action");
			}
			columns.get("topic").setSort(false);
			columns.remove("status");
			columns.remove("createdate");
			columns.remove("author");
			return columns;
		}

		@Override
		protected Map<Object, Object> getRowData(final Object dataObject) {
			final NewsBean news = (NewsBean) dataObject;
			final Map<Object, Object> rowData = new HashMap<Object, Object>();
			rowData.put("topic", getTitle(news));
			rowData.put("remarks", wrapNum(news.getRemarks()));
			rowData.put("views", wrapNum(news.getViews()));
			rowData.put("action", ACTIONc);
			return rowData;
		}

		private String getTitle(final NewsBean news) {
			final StringBuilder sb = new StringBuilder();
			sb.append("<div class=\"f3\" style=\"margin: 4px 0px;\">");
			final EContentStatus status = news.getStatus();
			if (status != EContentStatus.publish) {
				sb.append("<span style=\"margin-right: 6px;\" class=\"important-tip\">[ ")
						.append(status).append(" ]</span>");
			}
			final NewsCatalog catalog = getCatalogById(compParameter, news.getCatalogId());
			if (catalog != null) {
				sb.append("<a href=\"").append(
						getApplicationModule().getCatalogUrl(compParameter, catalog));
				sb.append("\">[").append(catalog.getText()).append("]</a>  ");
			}
			sb.append(wrapOpenLink(compParameter, news));
			sb.append("</div>");
			sb.append("<table cellpadding=\"0\" cellspacing=\"0\"><tr>");
			final String img = ContentUtils.getContentImage(compParameter, news);
			if (img != null) {
				sb.append("<td width=\"110\" valign=\"top\">").append(img).append("</td>");
			}
			sb.append("<td valign=\"top\">");
			sb.append("<div class=\"gray-color\">")
					.append(ContentUtils.getShortContent(news, 300, true)).append("</div>");
			sb.append("<div class=\"hsep\">");
			sb.append(ContentUtils.getAccountAware().wrapAccountHref(compParameter, news.getUserId(),
					news.getUserText()));
			sb.append(" , ").append(ConvertUtils.toDateString(news.getCreateDate()));
			sb.append(getImageByType(news.getTtype()));
			sb.append(NewsPagerUtils.getNewsContentUrl(compParameter, news));
			if ("attention".equals(compParameter.getRequestParameter("t"))) {
				sb.append(deleteAttention(EFunctionModule.news, news));
			}
			final IAccount account = AccountSession.getLogin(compParameter.getSession());
			if (account != null && account.getId().equals2(news.getUserId())
					&& status.ordinal() <= EContentStatus.audit.ordinal()) {
				final String cn = compParameter.componentBean.getName();
				sb.append("<span style=\"margin-left: 20px;\"><a onclick=\"$Actions['");
				sb.append(cn).append("'].edit(this);\">#(Edit)</a>");
				sb.append(HTMLBuilder.SEP).append("<a onclick=\"$Actions['");
				sb.append(cn).append("'].del(this);\">#(Delete)</a>");
				sb.append("</span>");
			}
			sb.append("</div>");
			sb.append("</td></tr></table>");
			return sb.toString();
		}
	}

	@Override
	public String getCatalogIdName(final PageRequestResponse requestResponse) {
		return getApplicationModule().getCatalogIdName(requestResponse);
	}

	@Override
	public NewsCatalog getCatalogById(final ComponentParameter compParameter, final Object id) {
		final List<NewsCatalog> catalogs = listNewsCatalog(compParameter);
		if (catalogs != null) {
			for (final NewsCatalog catalog : catalogs) {
				if (catalog.getId().equals2(id)) {
					return catalog;
				}
			}
		}
		return null;
	}

	@Override
	public List<NewsCatalog> listNewsCatalog(final ComponentParameter compParameter) {
		final List<NewsCatalog> catalogs = getApplicationModule().listNewsCatalog(compParameter);
		if (catalogs != null) {
			return catalogs;
		}
		return super.listNewsCatalog(compParameter);
	}

	@Override
	public String getRemarkHandleClass(final ComponentParameter compParameter) {
		return NewsRemarkHandle.class.getName();
	}

	@Override
	public EFunctionModule getFunctionModule() {
		return EFunctionModule.news;
	}
}
