package net.simpleframework.my.message;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.event.TableEntityAdapter;
import net.simpleframework.applets.notification.NotificationUtils;
import net.simpleframework.organization.IUser;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.organization.account.AccountSession;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class MessageAction extends AbstractAjaxRequestHandle {

	public IForward sentMessage(final ComponentParameter compParameter) {
		final IAccount login = AccountSession.getLogin(compParameter.getSession());
		if (login == null) {
			return null;
		}
		final ArrayList<SimpleMessage> al = new ArrayList<SimpleMessage>();
		final String[] users = StringUtils.split(
				compParameter.getRequestParameter("textMessageUsers"), ",");
		final String textBody = compParameter.getRequestParameter("textareaMessageEditor");
		if (users != null && users.length > 0 && StringUtils.hasText(textBody)) {
			for (final String user : users) {
				final IUser userObject = OrgUtils.um().getUserByName(user);
				if (userObject != null) {
					final SimpleMessage sMessage = new SimpleMessage();
					sMessage.setMessageType(EMessageType.user);
					sMessage.setSentId(login.getId());
					sMessage.setSentDate(new Date());
					sMessage.setTextBody(textBody);
					sMessage.setToId(userObject.getId());
					al.add(sMessage);
				}
			}
			if (al.size() > 0) {
				final ITableEntityManager temgr = MessageUtils
						.getTableEntityManager(SimpleMessage.class);
				temgr.insertTransaction(al.toArray(), new TableEntityAdapter() {
					@Override
					public void afterInsert(final ITableEntityManager manager, final Object[] objects) {
						for (final Object object : objects) {
							final SimpleMessage sMessage = (SimpleMessage) object;
							final StringBuilder tb = new StringBuilder();
							try {
								tb.append("<p>")
										.append(
												LocaleI18n.getMessage("MessageAction.3", login.user(),
														ConvertUtils.toDateString(sMessage.getSentDate())))
										.append("</p>");
								tb.append("<a href=\"")
										.append(
												MessageUtils.applicationModule.getApplicationUrl(compParameter))
										.append("\">").append(LocaleI18n.getMessage("MessageAction.2"))
										.append("</a>");
							} catch (final Exception e) {
								logger.warn(e);
							}
							NotificationUtils.createSystemMessageNotification(sMessage.getSentId(),
									sMessage.getToId(), LocaleI18n.getMessage("MessageAction.1"),
									tb.toString(), sMessage.getToId());
							compParameter.removeSessionAttribute(MyMessageList.SESSION_DELETE_MESSAGE);
						}
					}
				});
			}
		}
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				json.put("result", LocaleI18n.getMessage("MessageAction.0", al.size()));
			}
		});
	}

	public IForward deleteMessage(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				json.put(
						"result",
						MessageUtils.getTableEntityManager(SimpleMessage.class).delete(
								new ExpressionValue("id=?", new Object[] { compParameter
										.getRequestParameter("messageId") })) > 0);
			}
		});
	}
}
