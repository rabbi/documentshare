package net.simpleframework.web.page.component.ui.htmleditor;

import java.util.Locale;

import net.simpleframework.util.JavascriptUtils;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.AbstractComponentJavascriptRender;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ComponentRenderUtils;
import net.simpleframework.web.page.component.IComponentRegistry;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class HtmlEditorRender extends AbstractComponentJavascriptRender {
	public HtmlEditorRender(final IComponentRegistry componentRegistry) {
		super(componentRegistry);
	}

	@Override
	public String getJavascriptCode(final ComponentParameter compParameter) {
		final HtmlEditorBean htmlEditor = (HtmlEditorBean) compParameter.componentBean;

		final StringBuilder sb = new StringBuilder();
		final String actionFunc = htmlEditor.getActionFunction();
		final String textarea = htmlEditor.getTextarea();
		final boolean hasTextarea = StringUtils.hasText(textarea);
		sb.append("if (CKEDITOR._loading) return; CKEDITOR._loading = true;");
		if (hasTextarea) {
			sb.append(actionFunc).append(".editor = CKEDITOR.replace(\"");
			sb.append(textarea).append("\", {");
		} else {
			sb.append(ComponentRenderUtils.initContainerVar(compParameter));
			sb.append(actionFunc).append(".editor = CKEDITOR.appendTo(")
					.append(ComponentRenderUtils.VAR_CONTAINER).append(", {");
		}
		sb.append("contentsCss: [\"").append(getCssResourceHomePath(compParameter))
				.append("/contents.css").append("\"],");
		sb.append("smiley_path: \"").append(getResourceHomePath(compParameter)).append("/smiley/\",");
		sb.append("enterMode: ").append(getLineMode(htmlEditor.getEnterMode())).append(",");
		sb.append("shiftEnterMode: ").append(getLineMode(htmlEditor.getShiftEnterMode())).append(",");
		sb.append("language: \"").append(getLanguage()).append("\",");
		sb.append("autoUpdateElement: false,");
		sb.append("uiColor: \"#C9E3F5\",");
		sb.append("startupFocus: ").append(htmlEditor.isStartupFocus()).append(",");
		sb.append("toolbarCanCollapse: ").append(htmlEditor.isToolbarCanCollapse()).append(",");
		sb.append("resize_enabled: ").append(htmlEditor.isResizeEnabled()).append(",");
		sb.append(ComponentRenderUtils.jsonHeightWidth(compParameter));
		sb.append("on: {");
		sb.append("instanceReady: function(ev) { CKEDITOR._loading = false; ");
		final String jsLoadedCallback = (String) compParameter.getBeanProperty("jsLoadedCallback");
		if (StringUtils.hasText(jsLoadedCallback)) {
			sb.append(jsLoadedCallback);
		}
		sb.append("}");
		sb.append("},");
		sb.append("toolbar: \"").append(htmlEditor.getToolbar()).append("\"");
		sb.append("});");
		if (hasTextarea) {
			sb.append("$(\"").append(textarea).append("\").htmlEditor = ");
			sb.append(actionFunc).append(".editor;");
		}

		String htmlContent = null;
		final IHtmlEditorHandle handle = (IHtmlEditorHandle) compParameter.getComponentHandle();
		if (handle != null) {
			htmlContent = handle.getHtmlContent(compParameter);
		}
		if (!StringUtils.hasText(htmlContent)) {
			htmlContent = htmlEditor.getHtmlContent();
		}
		if (StringUtils.hasText(htmlContent)) {
			sb.append(actionFunc).append(".editor.setData(\"");
			sb.append(JavascriptUtils.escape(htmlContent)).append("\");");
		}

		final StringBuilder sb2 = new StringBuilder();
		sb2.append("var act = $Actions[\"").append(compParameter.getBeanProperty("name"))
				.append("\"];");
		sb2.append("if (act && act.editor) { CKEDITOR.remove(act.editor); }");
		return ComponentRenderUtils.wrapActionFunction(compParameter, sb.toString(), sb2.toString());
	}

	private String getLanguage() {
		final Locale l = LocaleI18n.getLocale();
		if (l.equals(Locale.SIMPLIFIED_CHINESE)) {
			return "zh-cn";
		} else {
			return "en";
		}
	}

	private String getLineMode(final ELineMode lineMode) {
		if (lineMode == ELineMode.br) {
			return "CKEDITOR.ENTER_BR";
		} else if (lineMode == ELineMode.div) {
			return "CKEDITOR.ENTER_DIV";
		} else {
			return "CKEDITOR.ENTER_P";
		}
	}
}
