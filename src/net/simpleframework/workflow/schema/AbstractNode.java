package net.simpleframework.workflow.schema;

import java.util.Iterator;

import net.simpleframework.core.AbstractElementBean;
import net.simpleframework.util.IConstants;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class AbstractNode extends AbstractElementBean {
	private final AbstractNode parent;

	public AbstractNode(final Element dom4jElement, final AbstractNode parent) {
		super(dom4jElement);
		this.parent = parent;
		parseElement();
	}

	public AbstractNode getParent() {
		return parent;
	}

	protected Iterator<?> children(final String name) {
		final Element element = getElement().element(name);
		final Iterator<?> it = element != null ? element.elementIterator() : null;
		return it != null ? it : IConstants.NULL_ITERATOR;
	}

	protected static Element addElement(final AbstractNode bean, final String parent,
			final String name) {
		final Element element = bean.getElement();
		Element p = element.element(parent);
		if (p == null) {
			p = element.addElement(parent);
		}
		return p.addElement(name);
	}
}
