package net.simpleframework.workflow.schema;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class RelativeJobEnum {

	public static enum ERef {
		/**
		 * 流程启动者
		 */
		processInitiator,

		/**
		 * 前一任务的参与者
		 */
		preActivityParticipant,

		/**
		 * 前一指定(名称)任务的参与者
		 */
		preNamedActivityParticipant
	}

	public static enum ERelation {
		/**
		 * 实际执行者
		 */
		actual,

		/**
		 * 相对于具体某一角色
		 */
		job,

		/**
		 * 相对于父角色
		 */
		parent,

		/**
		 * 相对于根角色
		 */
		root
	}

	public static enum EScope {
		/**
		 * 当前角色视图
		 */
		chart,

		/**
		 * 上级角色视图，视图名称相同
		 */
		parentChart
	}
}
