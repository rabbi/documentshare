package net.simpleframework.organization.component.userpager;

import java.util.HashMap;
import java.util.Map;

import net.simpleframework.applets.notification.MailMessageNotification;
import net.simpleframework.applets.notification.NotificationUtils;
import net.simpleframework.organization.IUser;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.organization.account.AccountContext;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.BeanUtils;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.util.StringUtils;
import net.simpleframework.util.script.ScriptEvalUtils;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.PageUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;
import net.simpleframework.web.page.component.base.ajaxrequest.AjaxRequestBean;
import net.simpleframework.web.page.component.ui.validatecode.DefaultValidateCodeHandle;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class UserUtilsAction extends AbstractAjaxRequestHandle {
	@Override
	public Object getBeanProperty(final ComponentParameter compParameter, final String beanProperty) {
		if ("selector".equals(beanProperty)) {
			final String handleMethod = ((AjaxRequestBean) compParameter.componentBean).getHandleMethod();
			if ("bindingMail".equals(handleMethod) || "sendBindingMail".equals(handleMethod)) {
				return "#bindingMail_form";
			} else if ("editPassword".equals(handleMethod)) {
				return "#_userpwd_form";
			} else if ("editUserBase".equals(handleMethod)) {
				return "#_userbase_form";
			} else if ("editUserAttr".equals(handleMethod)) {
				return "#_userattr_form";
			}
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	public IForward bindingMail(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				final String bindingMailCode = compParameter.getRequestParameter("textBindingMailCode");
				if (!StringUtils.hash(compParameter.getSession()).equals(bindingMailCode)) {
					json.put("bindingMailCode", LocaleI18n.getMessage("UserPagerAction.5"));
				} else {
					final IAccount account = OrgUtils.am().queryForObjectById(
							compParameter.getRequestParameter(OrgUtils.um().getUserIdParameterName()));
					account.setMailbinding(true);
					OrgUtils.am().update(account);
				}
			}
		});
	}

	public IForward sendBindingMail(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				if (!DefaultValidateCodeHandle.isValidateCode(compParameter.request, "textBindingMailValidateCode")) {
					json.put("validateCode", DefaultValidateCodeHandle.getErrorString());
				} else {
					final IUser user = OrgUtils.um().queryForObjectById(compParameter.getRequestParameter(OrgUtils.um().getUserIdParameterName()));
					final String email = compParameter.getRequestParameter("textSendBindingMail");
					if (!email.equals(user.getEmail())) {
						user.setEmail(email);
						OrgUtils.um().update(user);
					}

					final MailMessageNotification mailMessage = new MailMessageNotification();
					mailMessage.getTo().add(user);
					mailMessage.setSubject(LocaleI18n.getMessage("UserPagerAction.3", PageUtils.pageContext.getApplication().getApplicationConfig()
							.getTitle()));
					final Map<String, Object> variable = new HashMap<String, Object>();
					variable.put("username", user);
					variable.put("bindingcode", StringUtils.hash(compParameter.getSession()));
					mailMessage.setTextBody(ScriptEvalUtils.replaceExprFromResource(DefaultUserPagerHandle.class, "mail_binding.txt", variable));
					NotificationUtils.sendMessage(mailMessage);
					json.put("send", LocaleI18n.getMessage("UserPagerAction.4"));
				}
			}
		});
	}

	public IForward unBindingMail(final ComponentParameter compParameter) {
		final IAccount account = OrgUtils.am().queryForObjectById(compParameter.getRequestParameter(OrgUtils.um().getUserIdParameterName()));
		account.setMailbinding(false);
		OrgUtils.am().update(account);
		return null;
	}

	public IForward editUserBase(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				final IUser user = OrgUtils.um().queryForObjectById(compParameter.getRequestParameter(OrgUtils.um().getUserIdParameterName()));
				PageUtils.setObjectFromRequest(user, compParameter.request, "user_", UserPagerUtils.userProperties);
				user.setBirthday(ConvertUtils.toDate(compParameter.getRequestParameter("user_birthday"), IUser.birthdayDateFormat));
				OrgUtils.um().update(user);

				if (StringUtils.hasText(user.getMobile()) && StringUtils.hasText(user.getHometown()) && StringUtils.hasText(user.getDescription())) {
					AccountContext.update(user.account(), "org_edituser", user.getId());
				}
			}
		});
	}

	public IForward editUserAttr(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				final IAccount account = OrgUtils.am().queryForObjectById(compParameter.getRequestParameter(OrgUtils.um().getUserIdParameterName()));
				if (account != null) {
					BeanUtils.setProperty(account, "points",
							ConvertUtils.toInt(compParameter.getRequestParameter("user_points"), account.getPoints()));
					OrgUtils.am().update(account);
				}
			}
		});
	}

	public IForward editPassword(final ComponentParameter compParameter) {
		// final ComponentParameter nComponentParameter =
		// getComponentParameter(compParameter);
		// final IUserPagerHandle uHandle = (IUserPagerHandle)
		// nComponentParameter.getComponentHandle();
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				if (!DefaultValidateCodeHandle.isValidateCode(compParameter.request, "user_validateCode")) {
					json.put("validateCode", DefaultValidateCodeHandle.getErrorString());
				} else {
					final IAccount account = OrgUtils.am().queryForObjectById(
							compParameter.getRequestParameter(OrgUtils.um().getUserIdParameterName()));
					final String oldpassword = compParameter.getRequestParameter("user_old_password");
					if (!account.getPassword().equals(oldpassword)) {
						json.put("oldPassword", LocaleI18n.getMessage("UserPagerAction.1"));
					} else {
						account.setPassword(compParameter.getRequestParameter("user_password"));
						OrgUtils.am().update(account);
						if (ConvertUtils.toBoolean(compParameter.getRequestParameter("user_SendMail"), false)) {
							final MailMessageNotification mailMessage = new MailMessageNotification();
							mailMessage.getTo().add(account.user());
							mailMessage.setSubject(LocaleI18n.getMessage("UserPagerAction.2", PageUtils.pageContext.getApplication()
									.getApplicationConfig().getTitle()));
							final Map<String, Object> variable = new HashMap<String, Object>();
							variable.put("username", account.user());
							variable.put("oldpassword", oldpassword);
							variable.put("password", account.getPassword());
							mailMessage.setTextBody(ScriptEvalUtils.replaceExprFromResource(DefaultUserPagerHandle.class, "mail_edit_password.txt",
									variable));
							NotificationUtils.sendMessage(mailMessage);
						}
					}
				}
			}
		});
	}
}
