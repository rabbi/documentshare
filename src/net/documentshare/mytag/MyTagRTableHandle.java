package net.documentshare.mytag;

import java.util.HashMap;
import java.util.Map;

import net.documentshare.impl.AbstractCommonBeanAware;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.content.IContentApplicationModule;
import net.simpleframework.content.IContentPagerHandle;
import net.simpleframework.core.IApplicationModule;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.db.AbstractDbTablePagerHandle;

public class MyTagRTableHandle extends AbstractDbTablePagerHandle {
	@Override
	public IApplicationModule getApplicationModule() {
		return MyTagUtils.applicationModule;
	}

	@Override
	public Map<String, Object> getFormParameters(final ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, IContentPagerHandle._VTYPE);
		putParameter(compParameter, parameters, IMyTagApplicationModule._TAG_ID);
		return parameters;
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final IContentApplicationModule module = MyTagUtils.applicationModule.getContentApplicationModule(compParameter);
		if (module == null) {
			return null;
		}
		final ITableEntityManager tag_mgr = ItSiteUtil.getTableEntityManager(module);
		final SQLValue sqlValue = new SQLValue("select n.* from " + tag_mgr.getTablename() + " n inner join "
				+ MyTagUtils.getTableEntityManager(MyTagRBean.class).getTablename() + " r on r.rid = n.id where r.tagid=?",
				new Object[] { compParameter.getRequestParameter(IMyTagApplicationModule._TAG_ID) });
		return tag_mgr.query(sqlValue, module.getEntityBeanClass());
	}

	@Override
	protected Map<Object, Object> getTableRow(final ComponentParameter compParameter, final Object dataObject) {
		final AbstractCommonBeanAware news = (AbstractCommonBeanAware) dataObject;
		final Map<Object, Object> rows = new HashMap<Object, Object>();
		final StringBuilder sb = new StringBuilder();
		sb.append(news);
		sb.append("<div class=\"gray-color\" style=\"margin-top: 4px;\">");
		sb.append(ConvertUtils.toDateString(news.getCreateDate(), "yyyy-MM-dd"));
		sb.append(" By ").append(news.getUserText());
		sb.append("<a onclick=\"$Actions['_tagRDelete']('c=").append(news.getId());
		sb.append("&t=").append(compParameter.getRequestParameter(IMyTagApplicationModule._TAG_ID));
		sb.append("');\" style=\"margin-left: 20px;\">#(Delete)</a>");
		sb.append("</div>");
		rows.put("topic", sb.toString());
		return rows;
	}
}
