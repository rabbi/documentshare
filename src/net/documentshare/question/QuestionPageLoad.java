package net.documentshare.question;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.AbstractTitleAwarePageLoad;
import net.simpleframework.web.page.PageParameter;

public class QuestionPageLoad extends AbstractTitleAwarePageLoad {
	@Override
	public Object getBeanProperty(PageParameter pageParameter, String beanProperty) {
		if ("title".equals(beanProperty)) {
			final String questionId = pageParameter.getRequestParameter(QuestionUtils.questionId);
			if (StringUtils.hasText(questionId)) {
				final QuestionBean questionBean = QuestionUtils.applicationModule.getBean(QuestionBean.class,questionId);
				if (questionBean != null) {
					return wrapApplicationTitle(questionBean.getTitle() + " - 问答");
				}
			}
			return wrapApplicationTitle("问答");
		}
		return super.getBeanProperty(pageParameter, beanProperty);
	}

	/**
	 * 高级属性
	 * @throws Exception
	 */
	public void questionAttrLoad(final PageParameter pageParameter, final Map<String, Object> dataBinding, final List<String> visibleToggleSelector,
			final List<String> readonlySelector, final List<String> disabledSelector) throws Exception {
		final QuestionBean questionBean = QuestionUtils.getQuestionBean(pageParameter);
		if (questionBean != null) {
			dataBinding.put("questionId", questionBean.getId());
		}
	}

	/**
	 * 发布项目编辑
	 */
	public void questionLoad(final PageParameter pageParameter, final Map<String, Object> dataBinding, final List<String> visibleToggleSelector,
			final List<String> readonlySelector, final List<String> disabledSelector) throws Exception {
		final QuestionBean questionBean = QuestionUtils.getQuestionBean(pageParameter);
		if (questionBean != null) {
			dataBinding.put("questionId", questionBean.getId());
			dataBinding.put("q_title", questionBean.getTitle());
			dataBinding.put("q_content", questionBean.getContent());
			dataBinding.put("q_reward", questionBean.getReward());
			dataBinding.put("q_keywords", questionBean.getKeywords());
			dataBinding.put("q_limitTime", ConvertUtils.toDateString(questionBean.getLimitTime(), ItSiteUtil.yyyyMMdd_HH));
			dataBinding.put("q_email", questionBean.isEmail());
		} else {
			dataBinding.put("q_email", true);
			final Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 15);
			dataBinding.put("q_limitTime", ConvertUtils.toDateString(cal.getTime(), ItSiteUtil.yyyyMMdd_HH));
		}
	}

}
