package net.documentshare.question;

public enum EQuestionStatus {
	noraml() {
		@Override
		public String toString() {
			return "进行中...";
		}
	},
	time() {
		@Override
		public String toString() {
			return "时间过期";
		}
	},
	find() {
		@Override
		public String toString() {
			return "找到最佳答案";
		}
	},
	noGoodAnswer() {
		@Override
		public String toString() {
			return "无满意答案";
		}
	}
}
