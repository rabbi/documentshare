package net.documentshare.question;

import net.documentshare.impl.AbstractAttention;
import net.simpleframework.core.id.ID;

public class QuestionAttention extends AbstractAttention {
	private ID questionId;

	public void setQuestionId(ID questionId) {
		this.questionId = questionId;
	}

	public ID getQuestionId() {
		return questionId;
	}

	private static final long serialVersionUID = -4251173116301623873L;
}
