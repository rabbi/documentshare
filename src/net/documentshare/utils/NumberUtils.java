package net.documentshare.utils;

import net.simpleframework.util.ConvertUtils;

import org.springframework.util.StringUtils;

public class NumberUtils {
	public static double toDouble(final String s) {
		if (!StringUtils.hasText(s)) {
			return 0;
		}
		if (s.contains("%")) {
			return ConvertUtils.toDouble(s.replaceAll("%", ""), 0) / 100;
		}
		return ConvertUtils.toDouble(s, 0);
	}
}
