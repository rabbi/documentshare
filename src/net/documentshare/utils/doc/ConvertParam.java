package net.documentshare.utils.doc;

import java.io.File;

public class ConvertParam {
	public File pdfFile;
	public String outPath;
	public boolean splitPage;

	public boolean poly2bitmap;
	public String convertPages;

	public File getPdfFile() {
		return pdfFile;
	}

	public void setPdfFile(File pdfFile) {
		this.pdfFile = pdfFile;
	}

	public String getOutPath() {
		return outPath;
	}

	public void setOutPath(String outPath) {
		this.outPath = outPath;
	}

	public boolean isSplitPage() {
		return splitPage;
	}

	public void setSplitPage(boolean splitPage) {
		this.splitPage = splitPage;
	}

	public boolean isPoly2bitmap() {
		return poly2bitmap;
	}

	public void setPoly2bitmap(boolean poly2bitmap) {
		this.poly2bitmap = poly2bitmap;
	}

	public String getConvertPages() {
		return convertPages;
	}

	public void setConvertPages(String convertPages) {
		this.convertPages = convertPages;
	}

}
