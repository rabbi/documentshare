package net.documentshare.handle;

import net.simpleframework.organization.component.register.DefaultUserRegisterHandle;
import net.simpleframework.web.page.component.ComponentParameter;

public class DShareUserRegisterHandle extends DefaultUserRegisterHandle {

	@Override
	public String getJavascriptCallback(final ComponentParameter compParameter,
			final String jsAction, final Object bean) {
		String jsCallback = super.getJavascriptCallback(compParameter, jsAction, bean);
		if ("submit".equals(jsAction)) {
			jsCallback += "$Actions.loc('/login.html');";
		}
		return jsCallback;
	}
}
