package net.documentshare.docu;

import java.util.Date;

import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.core.id.ID;
import net.simpleframework.organization.IUser;
import net.simpleframework.organization.IUserBeanAware;

/**
 * 删除日志
 *
 */
public class DocuDelLog extends AbstractIdDataObjectBean implements IUserBeanAware {
	private String title;//文档
	private String delReason;//删除原因
	private ID userId;//用户
	private Date delDate;//删除日期
	private boolean status = false;//是否是用户

	public DocuDelLog() {
		this.delDate = new Date();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDelReason() {
		return delReason;
	}

	public void setDelReason(String delReason) {
		this.delReason = delReason;
	}

	public ID getUserId() {
		return userId;
	}

	public void setUserId(ID userId) {
		this.userId = userId;
	}

	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String getUserText() {
		final IUser user = ItSiteUtil.getUserById(userId);
		return user == null ? "" : user.getText();
	}

}
