package net.documentshare.docu;

import java.util.List;
import java.util.Map;

import net.documentshare.common.CommonUtils;
import net.documentshare.common.ECommonType;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.AbstractTitleAwarePageLoad;
import net.simpleframework.web.page.PageParameter;

public class DocuPageLoad extends AbstractTitleAwarePageLoad {
	@Override
	public Object getBeanProperty(PageParameter pageParameter, String beanProperty) {
		if ("title".equals(beanProperty)) {
			final String docuId = pageParameter.getRequestParameter(DocuUtils.docuId);
			if (StringUtils.hasText(docuId)) {
				final DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, docuId);
				if (docuBean != null) {
					return wrapApplicationTitle(docuBean.getTitle() + " - 文档");
				}
			}
			return wrapApplicationTitle("文档");
		}
		return super.getBeanProperty(pageParameter, beanProperty);
	}

	/**
	 * 高级属性
	 * 
	 * @throws Exception
	 */
	public void docuAttrLoad(final PageParameter pageParameter, final Map<String, Object> dataBinding, final List<String> visibleToggleSelector,
			final List<String> readonlySelector, final List<String> disabledSelector) throws Exception {
		final String docuId = pageParameter.getRequestParameter(DocuUtils.docuId);
		final DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, docuId);
		if (docuBean != null) {
			dataBinding.put("docu_status", docuBean.getStatus().name());
			dataBinding.put("docu_ttype", docuBean.getTtype().name());
			dataBinding.put("docu_grade", docuBean.getAdminGrade());
		}
		dataBinding.put("docuId", docuId);
	}

	/**
	 * 文档属性
	 * 
	 * @throws Exception
	 */
	public void docuAttrLoad1(final PageParameter pageParameter, final Map<String, Object> dataBinding, final List<String> visibleToggleSelector,
			final List<String> readonlySelector, final List<String> disabledSelector) throws Exception {
		final String docu_auto = CommonUtils.getContent(ECommonType.docu_auto);
		if ("true".equals(docu_auto)) {
			dataBinding.put("autoAudit", "true");
		}
		final String converNumber = CommonUtils.getContent(ECommonType.converNumber);
		if (StringUtils.hasText(converNumber)) {
			dataBinding.put("converNumber", converNumber);
		}
	}

	/**
	 * 文档编辑
	 */
	public void docuLoad(final PageParameter pageParameter, final Map<String, Object> dataBinding, final List<String> visibleToggleSelector,
			final List<String> readonlySelector, final List<String> disabledSelector) throws Exception {
		final String docuId = pageParameter.getRequestParameter(DocuUtils.docuId);
		final DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, docuId);
		if (docuBean != null) {
			dataBinding.put("docu_title", docuBean.getTitle());
			dataBinding.put("docu_content", docuBean.getContent());
			dataBinding.put("docu_keyworks", docuBean.getKeyworks());
			dataBinding.put("docu_point", docuBean.getPoint());
			dataBinding.put("docu_free_page",
					(docuBean.getAllowRead() >= 1 || docuBean.getAllowRead() == 0) ? docuBean.getAllowRead() : docuBean.getAllowRead() * 100 + "%");
			dataBinding.put("docu_catalog", docuBean.getCatalogId());
			final StringBuffer catalogTitle = new StringBuffer();
			final DocuCatalog catalog = DocuUtils.applicationModule.getBean(DocuCatalog.class, docuBean.getCatalogId());
			if (catalog != null) {
				final DocuCatalog pCatalog = (DocuCatalog) catalog.parent(DocuUtils.applicationModule);
				if (pCatalog != null) {
					catalogTitle.append(pCatalog.getText()).append("-");
				}
				catalogTitle.append(catalog.getText());
			}
			dataBinding.put("docu_catalog_text", catalogTitle.toString());

			final DocuCodeCatalog codeCatalog = DocuUtils.applicationModule.getBean(DocuCodeCatalog.class, docuBean.getCodeCatalogId());
			dataBinding.put("docu_code_catalog", docuBean.getCodeCatalogId());
			dataBinding.put("docu_code_catalog_text", codeCatalog == null ? "" : codeCatalog.getText());
			dataBinding.put("code_language", docuBean.getLanguage());

			dataBinding.put("docu_function", docuBean.getDocuFunction() == null ? "" : docuBean.getDocuFunction().name());
			// 管理员可以修改属性
			if (docuBean.getDocuFunction() == EDocuFunction.docu && !ItSiteUtil.isManage(pageParameter, DocuUtils.applicationModule)) {
				disabledSelector.add("#docu_function");
				readonlySelector.add("#docu_function");
			}
			if (docuBean.getDocuFunction() == EDocuFunction.code) {
				visibleToggleSelector.add(".code_catalog_id");
			}
			if (docuBean.getDocuFunction() != EDocuFunction.docu) {
				visibleToggleSelector.add(".docu_pages");
			}
			if (docuBean.getDocuFunction() == EDocuFunction.code) {
				visibleToggleSelector.add(".code_catalog_id");
			}
		}
		dataBinding.put("docuId", docuId);
	}

	/**
	* 上传文档编辑
	*/
	public void docuLoad1(final PageParameter pageParameter, final Map<String, Object> dataBinding, final List<String> visibleToggleSelector,
			final List<String> readonlySelector, final List<String> disabledSelector) throws Exception {
		final String docuId = pageParameter.getRequestParameter(DocuUtils.docuId);
		final DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, docuId);
		if (docuBean != null) {
			dataBinding.put("docu_title", docuBean.getTitle());
			dataBinding.put("docu_function", docuBean.getDocuFunction() == null ? "" : docuBean.getDocuFunction().name());
			//管理员可以修改属性
			if (docuBean.getDocuFunction() == EDocuFunction.docu) {
				disabledSelector.add("#docu_function");
				readonlySelector.add("#docu_function");
			}
			if (docuBean.getDocuFunction() == EDocuFunction.data) {
				visibleToggleSelector.add(".docu_pages");
			}
		}
		dataBinding.put("docuId", docuId);
	}
}
