package net.documentshare.docu.corpus;

import java.util.Date;
import java.util.Map;

import net.documentshare.docu.DocuUtils;
import net.documentshare.i.IItSiteApplicationModule;
import net.documentshare.impl.AbstractAttention;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.IDataObjectValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.event.TableEntityAdapter;
import net.simpleframework.content.EContentType;
import net.simpleframework.core.id.ID;
import net.simpleframework.organization.IJob;
import net.simpleframework.organization.account.AccountSession;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.BeanUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;

public class CorpusAajxHandle extends AbstractAjaxRequestHandle {
	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobExecute".equals(beanProperty)) {
			return IJob.sj_account_normal;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	public IForward addCorpus(final ComponentParameter compParameter) {
		final ID userId = ItSiteUtil.getLoginUser(compParameter).getId();
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final String corposId = compParameter.getRequestParameter("corpos_Id");
				final String name = compParameter.getRequestParameter("corpus_title");
				final String description = compParameter.getRequestParameter("description");
				final String catalogId = compParameter.getRequestParameter("docu_catalog");
				CorpusBean corpusBean = null;
				if (StringUtils.hasText(corposId)) {
					corpusBean = DocuUtils.applicationModule.getBean(CorpusBean.class, Integer.parseInt(corposId));
				} else {
					corpusBean = (CorpusBean) DocuUtils.applicationModule.getBeanByExp(CorpusBean.class, "userId='" + userId.getValue()
							+ "' and name='" + name + "'", null);
					if (corpusBean != null) {
						json.put("exist", "当前文辑已存在");
						return;
					}
					corpusBean = new CorpusBean();
				}
				corpusBean.setName(name);
				corpusBean.setCatalogId(ID.Utils.newID(Integer.parseInt(catalogId)));
				corpusBean.setUserId(userId);
				if (!StringUtils.hasText(corposId)) {
					corpusBean.setCreateDate(new Date());
				}
				corpusBean.setUpdateDate(new Date());
				final CorpusBean c = corpusBean;
				if (StringUtils.hasText(description))
					corpusBean.setDescription(description);
				DocuUtils.applicationModule.doUpdate(corpusBean, new TableEntityAdapter() {

					@Override
					public void afterInsert(ITableEntityManager manager, Object[] objects) {
						json.put("sucess", "文集创建成功");
						DocuUtils.applicationModule.createCorpusLuceneManager(compParameter).objects2DocumentsBackground(c);
					}

				});

			}

		});
	}

	public IForward setRecommendCorpus(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final String corposId = compParameter.getRequestParameter("corpusId");
				CorpusBean corpusBean = null;
				if (StringUtils.hasText(corposId)) {
					corpusBean = DocuUtils.applicationModule.getBean(CorpusBean.class, Integer.parseInt(corposId));
					if (corpusBean.getContentType() == EContentType.recommended) {
						corpusBean.setContentType(EContentType.normal);
					} else {
						corpusBean.setContentType(EContentType.recommended);
					}
					DocuUtils.applicationModule.doUpdate(corpusBean, new TableEntityAdapter() {
						@Override
						public void afterInsert(ITableEntityManager manager, Object[] objects) {
							json.put("sucess", "设置成功");
						}

					});

				}
			}
		});
	}

	public IForward deleteCorpus(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final String id = compParameter.getRequestParameter("corpusId");
				DocuUtils.applicationModule.doDelete(CorpusBean.class, id, new TableEntityAdapter() {

					@Override
					public void beforeDelete(ITableEntityManager manager, IDataObjectValue dataObjectValue) {
						DocuUtils.applicationModule.doDelete("CorpusId=" + id, CorpusOfDocBean.class);
						// 删除lucence
						DocuUtils.applicationModule.createLuceneManager(compParameter).deleteDocumentBackground(dataObjectValue.getValues());
					}

				});
				json.put("sucess", "删除成功");
			}

		});
	}

	/**
	 * 设置文集封面
	 * 
	 * @param compParameter
	 * @return
	 */
	public IForward updateFrontCover(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final String corpusId = compParameter.getRequestParameter("corpusId");
				final String frontCoverId = compParameter.getRequestParameter("frontCoverId");
				CorpusBean corpusBean = (CorpusBean) DocuUtils.applicationModule.getBean(CorpusBean.class, corpusId);
				if (corpusBean != null) {
					corpusBean.setFrontCover(ID.Utils.newID(Integer.parseInt(frontCoverId)));
					DocuUtils.applicationModule.doUpdate(corpusBean);
				}
			}

		});
	}

	/**
	 * 关注
	 * 
	 * @param compParameter
	 * @return
	 */
	public IForward corpusAttention(final ComponentParameter requestResponse) {
		return jsonForward(requestResponse, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final IAccount account = AccountSession.getLogin(requestResponse.getSession());
				final IItSiteApplicationModule application = DocuUtils.applicationModule;
				final String field = "corpusId";
				if (account == null)
					return;
				synchronized (DocuUtils.applicationModule) {
					final String id = requestResponse.getRequestParameter(field);
					final AbstractAttention attention = CorpusUtils.getAttention(account.getId(), id);
					if (attention == null) {
						try {
							final AbstractAttention attention1 = new CorpusAttention();
							attention1.setUserId(account.getId());
							BeanUtils.setProperty(attention1, field, id);
							attention1.setCreateDate(new Date());
							application.doUpdate(attention1, new TableEntityAdapter() {
								@Override
								public void afterInsert(ITableEntityManager manager, Object[] objects) {
									final CorpusBean aBean = (CorpusBean) application.getBean(CorpusBean.class, id);
									aBean.setAttentions(aBean.getAttentions() + 1);
									application.doUpdate(new Object[] { "attentions" }, aBean);
									json.put("act", "add");
								}
							});
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						application.doDelete(attention, new TableEntityAdapter() {
							public void afterDelete(ITableEntityManager manager, IDataObjectValue dataObjectValue) {
								final CorpusBean aBean = application.getBean(CorpusBean.class, BeanUtils.getProperty(attention, field));
								aBean.setAttentions(aBean.getAttentions() - 1);
								application.doUpdate(new Object[] { "attentions" }, aBean);
								json.put("act", "del");
							}
						});
					}
				}
			}
		});
	}

}
