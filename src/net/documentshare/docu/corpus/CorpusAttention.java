package net.documentshare.docu.corpus;

import net.documentshare.impl.AbstractAttention;
import net.simpleframework.core.id.ID;

public class CorpusAttention extends AbstractAttention {
	private ID corpusId;

	public ID getCorpusId() {
		return corpusId;
	}

	public void setCorpusId(ID corpusId) {
		this.corpusId = corpusId;
	}

}