package net.documentshare.docu;

import net.simpleframework.core.AbstractInitializer;
import net.simpleframework.core.IApplication;
import net.simpleframework.core.IInitializer;

/**
 *
 */
public class DocuInitializer extends AbstractInitializer {
	private IDocuApplicationModule websiteApplicationModule;

	@Override
	public void doInit(IApplication application) {
		try {
			super.doInit(application);
			websiteApplicationModule = new DocuApplicationModule();
			websiteApplicationModule.init(this);
			IInitializer.Utils.deploySqlScript(getClass(), application, DocuUtils.deployPath);
		} catch (Exception e) {
		}
	}
}
