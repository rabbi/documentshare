package net.documentshare.docu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.documentshare.mytag.MyTagRBean;
import net.documentshare.mytag.MyTagUtils;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.content.EContentStatus;
import net.simpleframework.content.EContentType;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.IJob;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.WebUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractPagerHandle;

public class DocuPaperHandle extends AbstractPagerHandle {
	@Override
	public Object getBeanProperty(final ComponentParameter compParameter, final String beanProperty) {
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final String c = WebUtils.toLocaleString(compParameter.getRequestParameter("c"));
		if (StringUtils.hasText(c)) {
			return DocuUtils.applicationModule.createLuceneManager(compParameter).getLuceneQuery(c);
		}
		final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager();
		final int catalogId = ConvertUtils.toInt(compParameter.getRequestParameter("catalogId"), 0);
		final List<Object> ol = new ArrayList<Object>();
		final StringBuffer sql = new StringBuffer();
		final StringBuffer where = new StringBuffer();
		final String _docu_topic = compParameter.getRequestParameter("_docu_topic");
		final String _docu_function = compParameter.getRequestParameter("_docu_function");
		final String _docu_catalog = compParameter.getRequestParameter("_docu_catalog");
		int s = 1;
		if (StringUtils.hasText(compParameter.getRequestParameter("tt"))) {
			s = ConvertUtils.toInt(compParameter.getRequestParameter("tt"), 1);
		} else if (StringUtils.hasText(compParameter.getRequestParameter("s"))) {
			s = ConvertUtils.toInt(compParameter.getRequestParameter("s"), 1);
		}
		final int od = ConvertUtils.toInt(compParameter.getRequestParameter("od"), 0);
		sql.append("select d.* from ");
		sql.append(DocuApplicationModule.docu_documentshare.getName()).append(" d ");
		if (StringUtils.hasText(_docu_topic)) {
			where.append(" and title like ?");
			ol.add("%" + _docu_topic + "%");
		}
		if (StringUtils.hasText(_docu_function)) {
			where.append(" and docuFunction=?");
			ol.add(EDocuFunction.whichOne(_docu_function));
		} else if (s != 0) {
			where.append(" and docuFunction=?");
			ol.add(EDocuFunction.whichOrdinal(s - 1));
		}
		where.append(" and status=?");
		ol.add(EContentStatus.publish);
		if (od == EDocuType2.recommended.ordinal()) {
			where.append(" and d.ttype=?");
			ol.add(EContentType.recommended);
		}
		final String tagId = compParameter.getRequestParameter("tagId");
		if (StringUtils.hasText(tagId)) {
			sql.append(" left join ").append(MyTagUtils.getTableEntityManager(MyTagRBean.class).getTablename()).append(" b");
			sql.append(" on d.id = b.rid ");
			where.append(" and  b.tagid=? ");
			ol.add(tagId);
		}
		if (catalogId != 0) {
			where.append(" and catalogId in(").append(DocuUtils.getJoinCatalog(catalogId)).append(")");
		} else if (StringUtils.hasText(_docu_catalog)) {
			where.append(" and d.catalogid in(").append(StringUtils.join(_docu_catalog.split(";"), ",")).append(")");
		}
		sql.append(" where 1=1 ").append(where);
		sql.append(" order by ttop desc");
		if (EDocuType2.popular.ordinal() == od) {
			sql.append(",views desc");
		} else if (EDocuType2.news.ordinal() == od) {
			sql.append(",createDate desc");
		} else if (EDocuType2.grade.ordinal() == od) {
			sql.append(",totalGrade desc");
		} else if (EDocuType2.vote.ordinal() == od) {
			sql.append(",votes desc");
		} else if (EDocuType2.remark.ordinal() == od) {
			sql.append(",remarkDate desc");
		} else if (EDocuType2.attention.ordinal() == od) {
			sql.append(",attentions desc");
		} else if (EDocuType2.download.ordinal() == od) {
			sql.append(",downCounter desc");
		}
		return tMgr.query(new SQLValue(sql.toString(), ol.toArray(new Object[] {})), DocuBean.class);
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "catalogId");
		putParameter(compParameter, parameters, "catalogPId");
		putParameter(compParameter, parameters, "_docu_topic");

		putParameter(compParameter, parameters, "tagId");

		putParameter(compParameter, parameters, "t");
		putParameter(compParameter, parameters, "s");
		putParameter(compParameter, parameters, "tt");
		putParameter(compParameter, parameters, "od");
		putParameter(compParameter, parameters, "c");
		return parameters;
	}

}
