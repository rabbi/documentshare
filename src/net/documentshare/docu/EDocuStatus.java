package net.documentshare.docu;

public enum EDocuStatus {
	audit {
		@Override
		public String toString() {
			return "审核";
		}
	},
	edit {
		@Override
		public String toString() {
			return "编辑";
		}
	},

	publish {
		@Override
		public String toString() {
			return "发布";
		}
	},

	delete_user {
		@Override
		public String toString() {
			return "申请删除";
		}
	},

	delete_manager {
		@Override
		public String toString() {
			return "不合格删除";
		}
	}
}